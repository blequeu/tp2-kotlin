package fr.esimed.tp

import kotlin.math.PI

// abstraite car on ne sait pas encore comment calculer le périmètre d'un polygone
abstract class Figure{
    abstract fun perimetre() : Double
}

class Cercle(private val rayon:Double) : Figure(){
    override fun perimetre(): Double {
        return 2 * PI * this.rayon
    }

    override fun toString(): String {
        return "Cercle (R=${this.rayon})"
    }
}

// open pour autoriser l'héritage.
// n'est pas une classe abstraite car il y a du code à l'intérieur (on sait calculer le périmétre d'un polygone)
open class Polygone(private val cotes:DoubleArray):Figure(){
    override fun perimetre(): Double {
        return cotes.sum()
    }
}

class Triangle(private val cote1:Double, private val cote2:Double, private val cote3:Double):Polygone(doubleArrayOf(cote1,cote2,cote3)){
    override fun toString(): String {
        return "Triangle ($cote1, $cote2, $cote3)"
    }
}

fun affichePerimetre(figure:Figure){
    println("Périmètre de ${figure.toString()} : ${figure.perimetre()}")
}

fun main(args: Array<String>) {
    val cercle1 = Cercle(5.0)
    val triangle1 = Triangle(3.0,4.0,4.0)

    affichePerimetre(cercle1)
    affichePerimetre(triangle1)
}