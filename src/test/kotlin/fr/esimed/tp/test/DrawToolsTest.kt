package fr.esimed.tp.test

import fr.esimed.tp.Cercle
import fr.esimed.tp.Triangle
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable


class DrawToolsTest{
    @Test
    fun figureTest(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(31.41592653589793, Cercle(5.0).perimetre()) },
            Executable { Assertions.assertEquals(11.0, Triangle(3.0,4.0,4.0).perimetre()) }
        )
    }
}
